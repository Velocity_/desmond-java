package nl.bartpelle.desmond.model.upstream;

/**
 * Created by Bart on 1/11/2015.
 */
public class PlayerCountResponse {

	private final int count;

	public PlayerCountResponse(int count) {
		this.count = count;
	}

}
