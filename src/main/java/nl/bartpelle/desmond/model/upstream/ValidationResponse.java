package nl.bartpelle.desmond.model.upstream;

/**
 * Created by Bart on 1/11/2015.
 *
 * A simple response for validation.
 */
public class ValidationResponse {

	private final boolean valid;

	public ValidationResponse(boolean valid) {
		this.valid = valid;
	}

}
