package nl.bartpelle.desmond;


import com.google.gson.Gson;
import nl.bartpelle.desmond.model.upstream.PlayerCountResponse;
import nl.bartpelle.desmond.model.upstream.ValidationResponse;
import spark.ResponseTransformer;

import java.util.Base64;
import java.util.Objects;
import static spark.Spark.*;

/**
 * <p>Created by Bart on 02/01/15.</p>
 *
 * <p>The DesmondApiServer class is the main entry point for a server owner to start their web functionality.
 * As soon as {@link #boot(String)} is called, the server starts accepting connections if and only if the
 * requesting party is making their API calls with the specified API key.</p>
 */
public class DesmondApiServer {

	/**
	 * A secret key string, used to protect API calls from malicious users.
	 */
	private String secret;

	/**
	 * The API implementation to pass the calls to and obtain information from. As this is entirely
	 * server-specific, the implementations will be served as extensions instead of built-in classes. This
	 * makes it easier for a user to have his/her own implementation for a function or even add his/her own ones.
	 */
	private DesmondImplementation implementation;

	/**
	 * The json serializer which will be used to return values from API calls.
	 */
	private Gson gson = new Gson();

	public DesmondApiServer(DesmondImplementation implementation) {
		this.implementation = implementation;
	}

	/**
	 * Start up a new Desmond API server which accepts calls from the Desmond Web Interface.
	 *
	 * @param secret A secret key, only known by you which <i><b>has to be equal</b></i> to your key
	 *               configured in your Desmond Web configuration.
	 */
	public void boot(String secret) {
		Objects.requireNonNull(secret);

		/* Require the secret to be smaller than 33 characters */
		if (secret.isEmpty() || secret.length() > 32)
			throw new IllegalArgumentException("secret key cannot be empty, nor be longer than 32 characters");

		this.secret = Base64.getUrlEncoder().encodeToString(secret.getBytes());

		port(10420);

		/* ACAO header to allow remote API calls */
		before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));

		get("/verify/*", (req, res) -> new ValidationResponse(validateSecret(req.splat()[0])), toJson());
		get("/api/players/count.json", (req, res) -> new PlayerCountResponse(implementation.getPlayerCount()), toJson());
	}

	/**
	 * Shuts down Desmond, closing the underlying Spark server and clearing all routes.
	 */
	public void shutdown() {
		stop();
	}

	/**
	 * Validates a secret key, both in Base64-format and in plain format (which automatically Base64 encodes
	 *  the given secret to check against the stored one.
	 * @param input The secret key to check, whether plain text or Base64.
	 * @return <code>true</code> if the secret is accepted, <code>false</code> otherwise.
	 */
	public boolean validateSecret(String input) {
		return input != null && (secret.equals(input) || secret.equals(Base64.getUrlEncoder().encodeToString(input.getBytes())));
	}

	/**
	 * Quick transformer for passing objects through the wire as json.
	 * @return A ResponseTransformer which turns the raw object into a json string.
	 */
	private ResponseTransformer toJson() {
		return gson::toJson;
	}

}
