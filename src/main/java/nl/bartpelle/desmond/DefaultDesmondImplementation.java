package nl.bartpelle.desmond;

import nl.bartpelle.desmond.exception.MissingImplementationException;

/**
 * Created by Bart on 1/6/2015.
 *
 */
public abstract class DefaultDesmondImplementation implements DesmondImplementation {

	@Override
	public int getPlayerCount() throws MissingImplementationException {
		throw new MissingImplementationException("getPlayerCount MUST be overridden and implemented as the theme relies on it!");
	}
}
