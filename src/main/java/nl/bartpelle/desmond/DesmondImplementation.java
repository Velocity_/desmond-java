package nl.bartpelle.desmond;

import nl.bartpelle.desmond.exception.MissingImplementationException;

/**
 * Created by Bart on 1/6/2015.
 */
public interface DesmondImplementation {

	/**
	 * Fetches the number of active players in the server to serve the <code>/api/players/count</code>
	 * request.
	 * @return The number of active players in the game. If the server has introduced a lobby of whatever
	 *         kind, this method should <i>not</i> include the lobby players.
	 * @throws MissingImplementationException if this feature has not been implemented.
	 */
	public int getPlayerCount() throws MissingImplementationException;

}
