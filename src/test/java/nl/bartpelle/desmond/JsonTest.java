package nl.bartpelle.desmond;

import com.google.gson.Gson;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Bart on 1/11/2015.
 */
public class JsonTest {

	@Test
	public void testJsonEncoding() {
		Gson gson = new Gson();

		Coder coder = new Coder();
		coder.name = "Bart";
		coder.money = Integer.MAX_VALUE;
		coder.problems = new Exception[] {new Exception("too rich")};

		String result = gson.toJson(coder);
		assertEquals(result, "{\"name\":\"Bart\",\"money\":2147483647,\"problems\":" +
				"[{\"detailMessage\":\"too rich\",\"stackTrace\":[],\"suppressedExceptions\":[]}]}");

		Coder thatsMe = gson.fromJson(result, Coder.class);
		assertNotNull(thatsMe);
		assertEquals(coder.name, thatsMe.name);
		assertEquals(coder.money, thatsMe.money);

		 /* Deep checking won't work cos of the nature of an exception */
		assertNotNull(thatsMe.problems);
		assertEquals(coder.problems.length, thatsMe.problems.length);
	}

	static class Coder {
		public String name;
		public int money;
		public Exception[] problems;
	}

}
