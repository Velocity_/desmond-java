package nl.bartpelle.desmond;

import org.junit.Test;
import spark.Spark;

import java.util.Base64;

import static org.junit.Assert.*;

/**
 * Created by Bart on 02/01/15.
 */
public class ConfigurationTest {

	@Test
	public void testApiSecretValidation() {
		DesmondApiServer server = new DesmondApiServer(new TestableImplementation());
		server.boot("Password123");

		assertTrue(server.validateSecret("Password123"));
		assertFalse(server.validateSecret("wrong"));
		assertTrue(server.validateSecret(Base64.getEncoder().encodeToString("Password123".getBytes())));
		assertFalse(server.validateSecret(Base64.getEncoder().encodeToString("password123".getBytes())));
		assertFalse(server.validateSecret(Base64.getEncoder().encodeToString("invalid".getBytes())));
		assertFalse(server.validateSecret(null));

		server.shutdown();
	}

	@Test(expected = NullPointerException.class)
	public void testNonNullBootParameter() {
		new DesmondApiServer(new TestableImplementation()).boot(null);
	}

}
