package nl.bartpelle.desmond;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import spark.Spark;

import java.io.IOException;
import java.util.Base64;

import static junit.framework.TestCase.*;

/**
 * Created by Bart on 1/8/2015.
 *
 */
public class SecretTest {

	@Test
	public void testValidationThroughUrl() throws IOException {
		DesmondApiServer server = new DesmondApiServer(new TestableImplementation());
		server.boot("Password123");

		/* Attempt the right one  */
		assertTrue(Boolean.parseBoolean(EntityUtils.toString(makeRequest("http://localhost:10420/verify/Password123"))));

		/* And a pre-encoded one */
		String password = Base64.getUrlEncoder().encodeToString("Password123".getBytes());
		assertTrue(Boolean.parseBoolean(EntityUtils.toString(makeRequest("http://localhost:10420/verify/" + password))));

		/* Try a bad one */
		assertFalse(Boolean.parseBoolean(EntityUtils.toString(makeRequest("http://localhost:10420/verify/incorrect"))));

		server.shutdown();
	}

	private HttpEntity makeRequest(String uri) throws IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet request = new HttpGet(uri);
		CloseableHttpResponse response = client.execute(request);

		return response.getEntity();
	}

}
